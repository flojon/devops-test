FROM microsoft/dotnet:2.1-sdk AS build
WORKDIR /app

# copy csproj and restore as distinct layers
COPY *.csproj ./
RUN dotnet restore

# copy everything else and build app
COPY . ./
RUN dotnet build

# Run tests
FROM build as test
RUN dotnet test

# run publish
FROM build AS publish
RUN dotnet publish -c Release -o publish /p:PublishWithAspNetCoreTargetManifest="false"

# Create runtime image
FROM microsoft/dotnet:2.1-aspnetcore-runtime AS runtime
WORKDIR /app
COPY --from=publish /app/publish ./
ENV ASPNETCORE_URLS=http://+:5000
ENTRYPOINT ["dotnet", "devops-test.dll"]
